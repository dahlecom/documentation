# Controltower

3 namespaces in openshift

- `cttest` - test
- `ctprodtet` - prodtest/staging
- `controltower` - production
  
all DMZ, WAF exposed.

`anyuid` SCC applied to default serviceAcccount.

https://bitbucket.dahl.se/scm/con/controltower-deploy.git

### Components

- 4 application pods running the same backend image but with different entrypoints.
- 1 application frontend pod
- redis
- minio backed by netapp trident pv
- **NOTE:** job manifest that creates the minio bucket, needs to run once in a new env or if minio PV is recreated.
  
Application images pulled from vendor repo. Pullsecret holds credentials

ldap server is set in configmap `backend-env` and points to single DC `f00dcdknit01.zf.if.atcsg.net`

Postgres DB outside openshift
