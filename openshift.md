# Openshift

## Egress IPs

```shell
$ k get netnamespace | grep 10.99
dahl-dev                                           6301219    ["10.99.68.19"]
dahl-prd                                           5435910    ["10.99.67.17"]
dahl-qa                                            11674145   ["10.99.67.27"]
dahl-stage                                         12038203   ["10.99.67.28"]
kasp-dev                                           11652572   ["10.99.68.17"]
kasp-prd                                           11060290   ["10.99.67.18"]
kasp-qa                                            15652525   ["10.99.67.19"]
kasp-stage                                         8527710    ["10.99.67.20"]
konr-dev                                           1894954    ["10.99.68.20"]
konr-prd                                           10947733   ["10.99.67.21"]
konr-qa                                            7377173    ["10.99.67.22"]
konr-stage                                         12474396   ["10.99.67.23"]
opti-dev                                           8922814    ["10.99.68.18"]
opti-prd                                           12620647   ["10.99.67.24"]
opti-qa                                            12110366   ["10.99.67.25"]
opti-stage                                         14328737   ["10.99.67.26"]
```

### EgressCIDRs on hostsubnet resources.

```shell
$ k get hostsubnet stratus-857gm-dmz-app-gw-452gx -o yaml
apiVersion: network.openshift.io/v1
egressCIDRs:
- 10.99.67.0/25
[...]
```

## Ingress points

### External exposure

stratus.dahl.se (212.247.0.30) -NAT-> 10.225.9.58 

### Load balancer config

| Name | VIP | Origin server(s) |
| ----------- | ----------- | ----------- |
| dmz not in DNS | 10.225.9.58 | 10.99.67.2 - 10.99.67.14 |
| int.stratus.cloud.dahl.se	| 10.99.71.6 | 10.99.68.2 - 10.99.68.14 |
| *.apps.stratus.cloud.dahl.se | 10.99.71.10 | 10.99.68.226 - 10.99.68.237 |
| api.stratus.cloud.dahl.se | 10.99.71.2 | 10.99.68.247,.248,.249 |

### Router/Ingresscontroller

```shell
Instance                                 		IP		        Node
router-dmz-dedicated-ingress-695db54d9-4949n    10.99.67.9      stratus-857gm-dmz-app-gw-452gx
router-dmz-dedicated-ingress-695db54d9-8j7pr    10.99.67.13     stratus-857gm-dmz-app-gw-vfflz

router-int-dedicated-ingress-6fb489ffbd-5gwmd   10.99.68.6      stratus-857gm-int-app-gw-xmj97
router-int-dedicated-ingress-6fb489ffbd-t7ss6   10.99.68.7      stratus-857gm-int-app-gw-sml22

router-default-85b9cf5759-5wpqk                 10.99.68.230    stratus-857gm-infra-vrc9d
router-default-85b9cf5759-64jcb                 10.99.68.228    stratus-857gm-infra-5xbgd
router-default-85b9cf5759-jl2pv                 10.99.68.226    stratus-857gm-infra-6nvvb

# Special instance to host IPI keepalived ingress IP:
router-default-85b9cf5759-7kgnv                 10.99.68.252    stratus-857gm-worker-g796m
```

namespace labels control which router picks up routes configured in the namespace:

```shell
$ k get ns dahl-dev -o jsonpath='{.metadata.labels}{"\n"}'
{"ingress":"dedicated","zone":"int"}
$ k get ns dahl-qa -o jsonpath='{.metadata.labels}{"\n"}'
{"ingress":"dedicated","zone":"dmz"}
```